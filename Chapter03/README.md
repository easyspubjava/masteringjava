1. [무엇이든 담을 수 있는 제네릭(Generic) 프로그래밍](https://gitlab.com/easyspubjava/masteringjava/-/blob/main/Chapter03/03-01/README.md)

2. [<T extends 클래스> 사용하기](https://gitlab.com/easyspubjava/masteringjava/-/blob/main/Chapter03/03-02/README.md)

3. [제네릭 메서드 활용하기](https://gitlab.com/easyspubjava/masteringjava/-/blob/main/Chapter03/03-03/README.md)

4. [자바에서 제공되는 자료 구조 구현 클래스들 - 컬레션 프레임워크](https://gitlab.com/easyspubjava/masteringjava/-/blob/main/Chapter03/03-04/README.md)

5. [순차적으로 자료를 관리하는 List 인터페이스를 구현한 클래스와 그 활용](https://gitlab.com/easyspubjava/masteringjava/-/blob/main/Chapter03/03-05/README.md)

6. [Collection 요소를 순회하는 Iterator](https://gitlab.com/easyspubjava/masteringjava/-/blob/main/Chapter03/03-06/README.md)

7. [중복되지 않게 자료를 관리하는 Set인터페이스를 구현한 클래스와 그 활용](https://gitlab.com/easyspubjava/masteringjava/-/blob/main/Chapter03/03-07/README.md)

8. [정렬을 위해 Comparable과 Comparator 인터페이스 구현하기](https://gitlab.com/easyspubjava/masteringjava/-/blob/main/Chapter03/03-08/README.md)

9. [쌍(pair)으로 자료를 관리하는 Map 인터페이스를 구현한 클래스와 그 활용](https://gitlab.com/easyspubjava/masteringjava/-/blob/main/Chapter03/03-09/README.md)
