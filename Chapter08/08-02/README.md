# 02. 채팅 서버와 클라이언트 만들기


## 채팅 프로그램 만들기

![chat3](./img/chat3.png)


![chat1](./img/chat1.png)


![chat2](./img/chat2.png)

## 채팅 서버

ChatServer.java

```
public class ChatServer{
	public static ChatRoom room = new ChatRoom();
	
	public static void main(String[] args) throws IOException{
	
		ServerSocket sSocket = new ServerSocket(20000);
		System.out.println("chatting start...");
		while(true){
			Socket socket = sSocket.accept(); //클라이언트 접속 대기
			System.out.println(socket + "입장"); 
			ChatRunner cr = new ChatRunner(socket); //ChatRunner 생성
			ChatServer.room.addChatRunner(cr); //ChatRunner 생성
			cr.start(); //ChatRunner 스레드 시작
		}
	}
}
```
ChatRunner.java
```
public class ChatRunner extends Thread{
	private boolean flag = false;
	private Socket socket = null;
	private BufferedReader br = null;
	private BufferedWriter bw = null;
	public ChatRunner(Socket socket) throws IOException{
		this.socket = socket;
		br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
	}
	public void run(){
		try{
			while(!this.flag){
				String msg = br.readLine();
				if(msg!=null && !msg.equals("")){
					ChatServer.room.sendMsgAll(msg);
				}else{
					this.flag = true;
				}
			}
			ChatServer.room.removeChatRunner(this);
			br.close();
			bw.close();
			socket.close();
		}catch(IOException e){e.printStackTrace();}
	}
	public void sendMessage(String msg) throws IOException{
		this.bw.write(msg + "\n");
		this.bw.flush();
	}
	public String toString(){
		return socket.toString();
	}
} 
```

ChatRoom.java
```
public class ChatRoom extends ArrayList<ChatRunner>{
	
	public synchronized void addChatRunner(ChatRunner obj){	
		this.add(obj);
	}
	public synchronized void removeChatRunner(Object obj){
		if(this.contains(obj)){
			this.remove(obj);
		}
	}
	public synchronized void sendMsgAll(String msg){
		Iterator<ChatRunner> ir = iterator();
		while(ir.hasNext()){
			ChatRunner c = ir.next();
			try{
				c.sendMessage(msg);
			}catch(IOException e){
				System.out.println(c.toString() + "의 메시지 전송 에러");
			}
		}
	}
} 
```

## 채팅 클라이언트

ChatClient.java

```
public class ChatClient extends Frame implements Runnable, ActionListener{
	private String nickname = null; //닉네임
	private String servAddr = null; //서버 주소
	private int servPort = 0; //서버 포트
	private boolean flag = false;
	private Socket socket = null; //클라이언트 소켓
	private BufferedReader br = null; //입력 스트림
	private BufferedWriter bw = null; //출력 스트림
	private TextField tf1 = new TextField();
	private TextArea ta1 = new TextArea();
	
	public ChatClient(String nickname, String servAddr, int servPort)
                                                     throws IOException{
		this.nickname = nickname;
		this.servAddr = servAddr;
		this.servPort = servPort;
		this.initGraphics(); //그래픽작업과 이벤트 등록 초기화
		this.initNetwork(); //소켓 생성과 스트림 작업 초기화와 스레드 시작
		new Thread(this).start();
	}
	public void initGraphics(){
		this.tf1.setBackground(Color.orange);
		this.ta1.setBackground(Color.yellow);
		this.add("North", tf1);
		this.add("Center", ta1);
		this.tf1.addActionListener(this);
		this.addWindowListener(
			new WindowAdapter(){
				public void windowClosing(WindowEvent e){
					System.exit(0);
					stop();
				}
			}
		);
	}
	//소켓과 입출력 스트림 초기화
	public void initNetwork() throws IOException{
		this.socket = new Socket(servAddr, servPort);
		bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		this.sendMessage("[" + nickname + "]:님이 입장했습니다.");
	}
	public void actionPerformed(ActionEvent e){
		String msg = tf1.getText();
		if(!msg.equals("")){
			this.sendMessage("[" + nickname + "]:" + msg);
			tf1.setText("");
		}
	}
	
	public void run(){
		try{
			while(!flag){
				String msg = br.readLine();
				if(msg != null && !msg.equals("")){
					this.ta1.append( msg +"\n");
				}else{
					flag = true;
				}
			}
			br.close();
			bw.close();
			socket.close();
		}catch(IOException e){e.printStackTrace();}
	}
	public void stop(){
		this.sendMessage("");
		this.flag = true;
	}
	public void sendMessage(String msg){
		try{
			this.bw.write(msg + "\n");
			this.bw.flush();
		}catch(IOException e){e.printStackTrace();}
	}
} 
```


ChatClientMain.java
```
public class ChatClientMain{
	
	public static void main(String[] args) throws IOException{
		
		ChatClient client;
		client = new ChatClient("봄", "127.0.0.1", 20000);
		client.setSize(300, 200);
		client.setTitle("봄");
		client.setVisible(true);
		
	} 
} 
```


