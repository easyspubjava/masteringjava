# 01. Echo Server 만들기


## 소켓 프로그래밍

### 소켓(Socket)이란
 네트워크로 연결된 두 대의 호스트간에 통신을 위한 양쪽 끝을 연결

### 소켓(Socket)의 역할
 컨넥션(Connection)을 개설하기 위한 도구
 전화기나 무전기???

### 대표적 Well Known Port    
7 : Echo

13 : DayTime

21 : Ftp

23 : Telnet    

25 : SMTP

80 : HTTP


![socket](./img/socket.png)

### ServerSocket

- ServerSocket

ServerSocket은 클라이언트와 통신할 수 있는 서버용 Socket을 만들어 준다.

- ServerSocket의 생성과 접속 대기

ServerSocket ss = new ServerSocket(10000);

Socket socket = ss.accept();

- ServerSocket의 accept()에서 리턴된 서버용 Socket

클라이언트 Socket으로 ServerSocket에 연결요청할 때 accept()가 반응한다.

ServerSocket이 accept()할 때 리턴된 서버용 Socket은 해당 클라이언트와 통신할 수 있는 유일한 수단이다.

accept()할 때 리턴된 서버용 Socket은 자동으로 포트(Port)를 할당받는다.

- 서버용 Socket의 생성 및 스트림 개설

```
	ServerSocket ss = new ServerSocket(10000);
	Socket socket = ss.accept();

	InputStream is = socket.getInputStream();
	OutputStream os = socket.getOutputStream();
```


```
public class EchoServerMain{

	public static void main (String args[]) throws IOException{
		ServerSocket ss = new ServerSocket (50000);
		System.out.println (50000 + " Port Echo Server Running...");
		while (true) {
			Socket socket = ss.accept();
			System.out.println (new Date().toString() + ":" + socket.toString());
			System.out.println (socket.getLocalPort());
			System.out.println (socket.getPort());
			BufferedReader br;
			BufferedWriter bw;
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

			String temp = br.readLine();
			bw.write(temp + " 1\n"); bw.flush();
			bw.write(temp + " 2\n"); bw.flush();
			bw.write(temp + " 3\n"); bw.flush();
			br.close();
			bw.close();
			socket.close();
		}
	} 
} 

public class EchoClientMain{

	public static void main (String args[]) throws IOException{
		Socket socket = new Socket("127.0.0.1", 50000);
		BufferedWriter bw;
		BufferedReader br;
		bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		bw.write("Hello, JAVA" + "\n");
		bw.flush();
		System.out.println(br.readLine());
		System.out.println(br.readLine());
		System.out.println(br.readLine());
		br.close();
		bw.close();		
		socket.close();
	} 
} 

```


